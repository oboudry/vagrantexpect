#!/usr/bin/expect

set path ./outfile.log
set var [open $path w]

log_user 0
set timeout 10
set userid  "<replace with user>"

# Comment this out if you want to prompt for password
set pass "<replace with password>"

# ############## Get two arguments - (1) Device (2) Command to be executed
set device  [lindex $argv 0]
set command [lindex $argv 1]

# # grab the password (uncomment if you want to prompt for password)
# stty -echo
# send_user -- "Password for $userid@$device: "
# expect_user -re "(.*)\n"
# send_user "\n"
# stty echo
# set pass $expect_out(1,string)

spawn ssh -l $userid $device
match_max [expr 32 * 1024]

expect {
  "Are you sure you want to continue connecting*" {
    send "yes\r"
    exp_continue
  }
  -re "\[Pp\]assword:" {send "$pass\r"}
}

expect {
   "> " {send "$command\r"}

   timeout {puts "Error reading prompt -> $expect_out(buffer)";exit}
}

expect "> "
set output $expect_out(buffer)
send "exit\r"
puts $var "$output\r\n"
