# Expect Vagrant Machine

Create a Centos/7 virtual machine with Expect installed and including a script
to run commands on NetApp.

## Getting Started

These instructions will get you up and running on your local machine for
development and testing purposes.

### Prerequisites

Install Virtualbox https://www.virtualbox.org/ 

Install Vagrant https://www.vagrantup.com/

Install git https://gitforwindows.org/

Clone the repository onto your local machine

```
git clone https://oboudry@bitbucket.org/oboudry/vagrantexpect.git
cd vagrantexpect
```

Provision the virtual machine, and login via ssh

```
vagrant up
vagrant ssh
```

Edit the ~/NetAppRunCommand.tcl file, replacing the username and password and run a NetApp command using it

```
./NetAppRunCommand netapp.local.dom "volume show"
```

Once you're finished playing with the VM, you can either suspend it or destroy it

```
vagrant suspend
vagrant destroy
```

## Authors

* **Olivier Boudry** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
